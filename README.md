[![pipeline status](https://gitlab.com/khalid.muhammad/t1ka/badges/master/pipeline.svg)](https://gitlab.com/khalid.muhammad/t1ka/commits/master)
[![coverage report](https://gitlab.com/khalid.muhammad/t1ka/badges/master/coverage.svg)](https://gitlab.com/khalid.muhammad/t1ka/commits/master)

# Anggota Kelompok
1. Ajrina Melynda Yofrizal (fitur 3: add friend)
2. Ismail (fitur 4: stats)
3. Khalid Muhammad (fitur 1: update status + fitur additional comment)
4. Yudho Prakoso (fitur 2: profile)


---

## Link Herokuapp
https://t1ka.herokuapp.com/