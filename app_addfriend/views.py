from django.shortcuts import render, redirect
from .forms import Addfriend_Form
from .models import Friend
# Create your views here.
response = {}
def index(request):
    html = 'addfriend.html'
    response['addfriend'] = Addfriend_Form
    response['friend'] = Friend.objects.all().order_by('date')
    return render(request,html,response)

def add_friend(request):
    form = Addfriend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friend(name=response['name'], url=response['url'])
        friend.save()
        return redirect ('/friends/')
