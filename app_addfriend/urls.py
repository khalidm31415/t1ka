from django.conf.urls import url, include
from .views import index, add_friend

urlpatterns = [
    url(r'^$',index, name="friends"),
    url(r'^add_friend/$',add_friend, name="add_friend"),
]
