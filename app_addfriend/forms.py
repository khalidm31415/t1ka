from django import forms

class Addfriend_Form(forms.Form):
    attrs = {
        'class': 'form-control'
    }

    name = forms.CharField(label='Name', required=True, max_length=30, widget=forms.TextInput(attrs=attrs))
    url = forms.URLField(label='URL',required=True,widget=forms.URLInput(attrs=attrs))
