from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_friend
from .models import Friend
from .forms import Addfriend_Form
from django.utils import timezone
from django.http import HttpRequest

# Create your tests here.
class FriendUnitTest(TestCase):

    def test_friend_is_exist(self):
        response = Client().get('/friends/')
        self.assertEqual(response.status_code,200)

    def test_friend_using_index_func(self):
        found = resolve('/friends/')
        self.assertEqual(found.func, index)

    def test_model_can_add_new_friend(self):
        new_friend = Friend.objects.create(name='name',url='http://gitu.com',date=timezone.now())

        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)

    def test_can_save_friend_list(self):
        response = self.client.post('/friends/add_friend/', data={'name': 'ajrina', 'url': 'http://gitu.com', 'date': timezone.now()})
        counting_all_available_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_available_friend, 1)
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/friends/')
        new_response = self.client.get('/friends/')
        html_response = new_response.content.decode('utf8')
        self.assertIn('ajrina', html_response)
        self.assertIn('http://gitu.com', html_response)
