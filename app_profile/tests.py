from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from .models import Profile
from django.http import HttpRequest

# Create your tests here.
class ProfileUnitTest(TestCase):

    def test_profile_is_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, index)

    def test_profile_is_male(self):
        make_profile = Profile.objects.create(
        name='Yudho Prakoso',
        gender=1,
        birth_date='1996-10-11',
        email='undomonxi@gmail.com',
        expertise='RPG',
        description='JRPG',
        )
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)

    def test_profile_is_female(self):
        make_profile = Profile.objects.create(
        name='Yudho Prakoso',
        gender=2,
        birth_date='1996-10-11',
        email='undomonxi@gmail.com',
        expertise='RPG',
        description='JRPG',
        )
        response = Client().get('/profile/')
        self.assertEqual(response.status_code,200)
