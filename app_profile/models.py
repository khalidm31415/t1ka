from django.db import models

# Create your models here.
class Profile(models.Model):
    MALE = 1
    FEMALE = 2
    GENDER_CHOICES = (
        (MALE, 'Male'),
        (FEMALE, 'Female'),
    )
    name = models.CharField(max_length=30, null='True')
    gender = models.PositiveSmallIntegerField(choices=GENDER_CHOICES, null='True')
    birth_date = models.DateField(null='True')
    email = models.EmailField(null='True')
    expertise = models.TextField(null='True')
    description = models.TextField(null='True')
