from django.shortcuts import render
from .models import Profile
# Create your views here.

def index(request):
    if Profile.objects.all().count()==0:
        Profile.objects.create(
            name="Tika Tikung",
            gender=2,
            birth_date="1997-5-26",
            email="adadeh@mail.com",
            expertise="Makan, Jalan-jalan",
            description="Nothing",
            )

    profil = Profile.objects.get(id=1)

    if profil.gender==1:
        profil.gender='Male'
    else:
        profil.gender='Female'

    response = {
    'name':profil.name,
    'gender':profil.gender,
    'birthdate':profil.birth_date,
    'email':profil.email,
    'expertise':[data.strip() for data in profil.expertise.split(',')],
    'description':profil.description,
    }
    html = 'profile.html'
    return render(request, html, response)
