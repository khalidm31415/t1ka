from django.conf.urls import url, include
from .views import index, update_status, comment_page, insert_comment

urlpatterns = [
    url(r'^$',index, name="status"),
    url(r'^update_status/$', update_status, name='update_status'),
    url(r'^comment_page/$', comment_page, name='comment_page'),
    url(r'^insert_comment/$', insert_comment, name='insert_comment')
]