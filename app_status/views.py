from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .forms import UpdateStatusForm, InsertCommentForm
from .models import Status, Comment
from app_profile.models import Profile

# Create your views here.
def index(request):
    response = {}
    html = 'status.html'
    response['update_status'] = UpdateStatusForm
    response['all_status'] = Status.objects.all().order_by('-date')
    response['all_comment'] = Comment.objects.all().order_by('-date')
    
    profil = Profile.objects.get(id=1)
    response['name'] = profil.name
    return render(request,html,response)

def update_status(request):
    response = {}
    form = UpdateStatusForm(request.POST or None, auto_id=False)
    if(request.method == 'POST' and form.is_valid()):
        response['content'] = request.POST['content']
        status = Status(content=response['content'])
        status.save()
        return redirect ('/status/')

def comment_page(request):
    response = {}
    html = 'comment.html'
    response['status_id'] = request.POST.get('status_id', None)
    response['comment_form'] = InsertCommentForm
    return render(request,html,response)

def insert_comment(request):
    response = {}
    form = InsertCommentForm(request.POST or None, auto_id=False)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['content'] = request.POST['content']
        response['status_id'] = request.POST['status_id']

        comment = Comment(name=response['name'] , content=response['content'], status_id=response['status_id'])
        comment.save()
        return HttpResponseRedirect ('/status/')
