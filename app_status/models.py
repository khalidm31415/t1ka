from django.db import models

# Create your models here.
class Status(models.Model):
    content = models.CharField(max_length=280)
    date = models.DateTimeField(auto_now_add=True)

class Comment(models.Model):
    name = models.CharField(max_length=40)
    content = models.CharField(max_length=280)
    date = models.DateTimeField(auto_now_add=True)
    status_id = models.IntegerField()
