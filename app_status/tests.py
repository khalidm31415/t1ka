from django.test import TestCase
from django.utils import timezone
from django.test import Client
from django.urls import resolve
from .views import index, update_status, comment_page
from .models import Status, Comment
from app_profile.models import Profile

# Create your tests here.
class UpdateStatus(TestCase):

    def test_app_status_url_exists(self):
        new_profile = Profile.objects.create()
        response = Client().get('/status/')
        self.assertEqual(response.status_code, 200)

        response = Client().get('/status/comment_page/')
        self.assertEqual(response.status_code, 200)

    def test_app_status_using_index_func(self):
        found = resolve('/status/')
        self.assertEqual(found.func, index)

        found = resolve('/status/comment_page/')
        self.assertEqual(found.func, comment_page)

    def test_model_can_create_new_status_and_comment(self):
        # Bikin status baru
        new_status = Status.objects.create(content='Status baru bro!', date=timezone.now())

        # Hitung semua status yang sudah dibuat
        counting_all_posted_status = Status.objects.all().count()
        self.assertEqual(counting_all_posted_status, 1)

        # Bikin komen ke status tadi
        new_comment = Comment.objects.create(name='Hepzibah Smith', content='Komen baru nih!', date=timezone.now(), status_id=new_status.id)

        # Hitung semua comment yang sudah dibuat
        counting_all_posted_comment = Comment.objects.all().count()
        self.assertEqual(counting_all_posted_comment, 1)

    def test_can_save_a_POST_request(self):
        # Ini status baru, jadi ada 1 baru untuk di-tes
        response = self.client.post('/status/update_status/', data={'content': 'Status baru coy!'})
        counting_all_status = Status.objects.all().count()
        self.assertEqual(counting_all_status, 1)

        new_status = Status.objects.create(content='Status baru bro!', date=timezone.now())
        response = self.client.post('/status/insert_comment/', data={'name': 'Tukang Komen',  'content': 'Komen baru coy!', 'date': timezone.now(), 'status_id':new_status.id})
        counting_all_comment = Comment.objects.all().count()
        self.assertEqual(counting_all_comment, 1)