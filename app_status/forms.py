from django import forms

class UpdateStatusForm(forms.Form):
    attrs = {
        'class': 'form-control',
        'placeholder': 'Apa yang sedang Anda pikirkan?'
    }

    content = forms.CharField(label='Update Status', required=True, max_length=280, widget=forms.TextInput(attrs=attrs))

class InsertCommentForm(forms.Form):
    attrs_name = {
        'class': 'form-control',
        'placeholder': 'Siapa namamu?'
    }

    attrs_content = {
        'class': 'form-control',
        'placeholder': 'Apa tanggapanmu tentang Status tadi?'
    }

    name = forms.CharField(label='Nama', required=True, max_length=40, widget=forms.TextInput(attrs=attrs_name))
    content = forms.CharField(label='Tulis komentar Anda di sini', required=True, max_length=280, widget=forms.TextInput(attrs=attrs_content))
    #status_id = forms.CharField(widget=forms.HiddenInput())