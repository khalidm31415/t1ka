from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index
from app_profile.models import Profile
from app_addfriend.models import Friend
from app_status.models import Status
from django.http import HttpRequest

# Create your tests here.
class StatsUnitTest(TestCase):

    def test_stats_is_exist(self):
        make_profile = Profile.objects.create(
        name='Yudho Prakoso',
        gender=1,
        birth_date='1996-10-11',
        email='undomonxi@gmail.com',
        expertise='RPG',
        description='JRPG',
        )
        make_status = Status.objects.create(
        content='Yudho Prakoso',
        )
        make_friend = Friend.objects.create(
        name='Yudho Prakoso',
        url='http://lola.com'
        )
        response = Client().get('/stats/')
        self.assertEqual(response.status_code,200)

    def test_using_index_func(self):
        found = resolve('/stats/')
        self.assertEqual(found.func, index)
