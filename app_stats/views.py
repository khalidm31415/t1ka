from django.shortcuts import render
from app_profile.models import Profile
from app_addfriend.models import Friend
from app_status.models import Status
# Create your views here.

def index(request):
    profil = Profile.objects.get(id=1)
    response = {
    'name': profil.name,
    'latest_post': Status.objects.latest('id'),
    'feed': Status.objects.all().count(),
    'friends': Friend.objects.all().count()
    }
    html = 'stats.html'
    return render(request, html, response)
