"""t1ka URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings
import app_status.urls as app_status
import app_profile.urls as app_profile
import app_addfriend.urls as app_addfriend
import app_stats.urls as app_stats
from app_status.views import index as index_status

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^status/', include(app_status, namespace="app_status")),
    url(r'^profile/', include(app_profile, namespace="app_profile")),
    url(r'^friends/', include(app_addfriend, namespace="app_addfriend")),
    url(r'^stats/', include(app_stats, namespace="app_stats")),
    url(r'^$', index_status, name='index')
]
